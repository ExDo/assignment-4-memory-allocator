#pragma once

#include <stdbool.h>

struct test_entity;

struct test_entity* create_test_entity(bool test_function(void), char const* test_name);

void destroy_test_entity(struct test_entity** entity);

void* get_test_function(struct test_entity const* entity);

char const* get_test_name(struct test_entity const* entity);