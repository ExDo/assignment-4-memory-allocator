#ifndef _TESTS_H_
#define _TESTS_H_

#include <stdbool.h>
#include <stdlib.h>

#include "test_entity.h"

bool simple_allocation_test(void);

bool free_single_block_test(void);

bool free_two_blocks_test(void);

bool new_region_after_old_test(void);

bool new_region_not_after_old_test(void);

void run_tests(size_t count, struct test_entity** tests);

#endif
