#define _GNU_SOURCE
#include "tests.h"

#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#define ONE_EIGHTH_OF_THE_HEAP 1024

static int8_t** allocate_bunch(size_t const len, size_t const one_element_size) {
    int8_t** allocated_memory_blocks = _malloc(len * sizeof(int8_t*));
    for (size_t i = 0; i < len; i++) allocated_memory_blocks[i] = _malloc(one_element_size);
    return allocated_memory_blocks;
}

static void free_bunch(int8_t** bunch, size_t const len) {
    for (size_t i = 0; i < len; i++) _free(bunch[i]);
    _free(bunch);
}

static size_t pages_count(size_t const mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t const mem) { return getpagesize() * pages_count(mem); }

static size_t region_actual_size(size_t const query) { return size_max(round_pages(size_from_capacity((block_capacity){query}).bytes), REGION_MIN_SIZE); }

bool simple_allocation_test() {
    void* heap = heap_init(REGION_MIN_SIZE);
    if (!heap) return false;

    bool return_result = false;

    int8_t* allocated_memory = _malloc(ONE_EIGHTH_OF_THE_HEAP);
    if (allocated_memory) {
        // If no segmentation fault, ok
        for (size_t i = 0; i < ONE_EIGHTH_OF_THE_HEAP; i++) allocated_memory[i] = (int8_t)i;

        return_result = true;
    }
    _free(allocated_memory);
    munmap(heap, region_actual_size(REGION_MIN_SIZE));
    return return_result;
}

bool free_single_block_test() {
    void* heap = heap_init(REGION_MIN_SIZE);
    if (!heap) return false;
    bool return_result = false;

    int8_t** allocated_memory_blocks = allocate_bunch(7, ONE_EIGHTH_OF_THE_HEAP);

    int8_t const* const addr = allocated_memory_blocks[2];
    _free(allocated_memory_blocks[2]);
    allocated_memory_blocks[2] = _malloc(ONE_EIGHTH_OF_THE_HEAP);
    if (allocated_memory_blocks[2] == addr) {
        return_result = true;
    }
    free_bunch(allocated_memory_blocks, 7);
    munmap(heap, region_actual_size(REGION_MIN_SIZE));
    return return_result;
}

bool free_two_blocks_test() {
    void* heap = heap_init(REGION_MIN_SIZE);
    if (!heap) return false;
    bool return_result = false;

    int8_t** allocated_memory_blocks = allocate_bunch(7, ONE_EIGHTH_OF_THE_HEAP);

    int8_t const* const addr1 = allocated_memory_blocks[2];
    int8_t const* const addr2 = allocated_memory_blocks[4];

    _free(allocated_memory_blocks[2]);
    _free(allocated_memory_blocks[4]);
    allocated_memory_blocks[2] = _malloc(ONE_EIGHTH_OF_THE_HEAP);
    allocated_memory_blocks[4] = _malloc(ONE_EIGHTH_OF_THE_HEAP);
    if (allocated_memory_blocks[2] == addr1 && allocated_memory_blocks[4] == addr2) {
        return_result = true;
    }
    free_bunch(allocated_memory_blocks, 7);
    munmap(heap, region_actual_size(REGION_MIN_SIZE));
    return return_result;
}

bool new_region_after_old_test() {
    void* heap = heap_init(REGION_MIN_SIZE);
    if (!heap) return false;
    bool return_result = false;

    int8_t* addr = _malloc(region_actual_size(REGION_MIN_SIZE) + REGION_MIN_SIZE);

    if (addr == HEAP_START + offsetof(struct block_header, contents)) {
        return_result = true;
    }
    _free(addr);
    munmap(heap, 2 * region_actual_size(REGION_MIN_SIZE));
    return return_result;
}

bool new_region_not_after_old_test() {
    void* heap = heap_init(REGION_MIN_SIZE);
    if (!heap) return false;
    void* mmap_addr = mmap(heap + region_actual_size(REGION_MIN_SIZE),
                           REGION_MIN_SIZE,
                           PROT_READ | PROT_WRITE,
                           MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE,
                           -1,
                           0);
    if (mmap_addr == MAP_FAILED) return false;
    if (mmap_addr != heap + region_actual_size(REGION_MIN_SIZE)) return false;

    bool return_result = false;

    int8_t* addr = _malloc(region_actual_size(REGION_MIN_SIZE) + REGION_MIN_SIZE);

    if (addr != HEAP_START + offsetof(struct block_header, contents)) {
        return_result = true;
    }
    _free(addr);
    munmap(heap, 2 * region_actual_size(REGION_MIN_SIZE));
    munmap(mmap_addr, REGION_MIN_SIZE);
    return return_result;
}

void run_tests(size_t const count, struct test_entity** tests) {
    for (size_t i = 0; i < count; i++) {
        bool (*const fun)(void) = get_test_function(tests[i]);
        char const* const func_name = get_test_name(tests[i]);
        bool const result = fun();
        fprintf(stdout, "Test %zu:\t%s\t\t%s\n", i + 1, func_name, result ? "PASSED" : "FAILED");
    }
}
