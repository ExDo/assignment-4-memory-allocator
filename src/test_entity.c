#include "test_entity.h"

#include "stdlib.h"

struct test_entity {
    bool (*test_function)();
    char const* test_name;
};

struct test_entity* create_test_entity(bool (*const test_function)(void), char const* const test_name) {
    struct test_entity* te = malloc(sizeof(struct test_entity));
    te->test_function = test_function;
    te->test_name = test_name;
    return te;
}

void destroy_test_entity(struct test_entity** entity) {
    if (entity && *entity) {
        free(*entity);
        *entity = NULL;
    }
}

void* get_test_function(struct test_entity const* const entity) {
    return entity->test_function;
}

char const* get_test_name(struct test_entity const* const entity) {
    return entity->test_name;
}
