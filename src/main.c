#include "test_entity.h"
#include "tests.h"

int main() {
    size_t const number_of_tests = 5;

    struct test_entity** tests = malloc(sizeof(struct test_entity*) * number_of_tests);

    tests[0] = create_test_entity(simple_allocation_test, "simple_allocation_test");
    tests[1] = create_test_entity(free_single_block_test, "free_single_block_test");
    tests[2] = create_test_entity(free_two_blocks_test, "free_two_blocks_test");
    tests[3] = create_test_entity(new_region_after_old_test, "new_region_after_old_test");
    tests[4] = create_test_entity(new_region_not_after_old_test, "new_region_not_after_old_test");

    run_tests(number_of_tests, tests);
    for (size_t i = 0; i < number_of_tests; i++)
        destroy_test_entity(&tests[0]);

    free(tests);

    return 0;
}
